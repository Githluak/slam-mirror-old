cp extract /usr/local/bin
cp pextract /usr/local/bin
chmod +x /usr/local/bin/extract && chmod +x /usr/local/bin/pextract
cp .netrc /root/.netrc
chmod 600 .netrc
chmod +x aria.sh
rm -rf .git
if [[ -n $ACCOUNTS_REPO_URL ]]; then
	git clone $ACCOUNTS_REPO_URL /usr/src/app/bot/accounts &> /dev/null
fi
python3 update.py && python3 -m bot
